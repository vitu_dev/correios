<?php
session_start();

require __DIR__ . "/vendor/autoload.php";

$mailer = new \Source\Classes\Email();
$pdf = new \Mpdf\Mpdf();

if (empty($_POST['objeto']) || empty($_POST['email'])) {
    \Source\Classes\Message::add(["Erro" => "Campos vazios"]);
    header("location: index.php");
}

$objeto = $_POST['objeto'];
$tracking = new \Source\Classes\Tracking($objeto);

$email = $_POST['email'];
$data = $tracking->getObjectInfo();
$_SESSION['dados'] = $data;

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    \Source\Classes\Message::add(["Erro" => "Formato de Email inválido"]);
    header("location: index.php");
}

$subjectsMessage = [
    "postado" => "O objeto foi Postado",
    "em trânsito" => "Objeto à caminho",
    "entrega" => "Objeto saiu para entrega",
    "entregue" => "Objeto Entregue",
    "Falha" => "Houve falha ao entregar Objeto",

];

$HtmlBody = "";
$arrayReversed = array_reverse($data);
foreach ($arrayReversed as $dado) {
    $local = $dado->local ?? $dado->destino;
    $localidade = isset($dado->origem) ? explode(" - ", $dado->origem)[1] : explode(" - ", $dado->local)[1];
    $HtmlBody .= "<tbody>
                      <tr>
                        <td>{$dado->data}<br>{$dado->hora}<br>
                            <label>{$localidade}</label>
                        </td>
                        <td>
                            <strong>{$dado->status}</strong><br>
                                    Registrado por {$local}
                        </td>
                     </tr>
                  </tbody>
                 ";
}

$html = getHTML(CSS_TABLE_EMAIL, $HtmlBody);
$pdf->WriteHTML($html);
$nameFile = "Relatorio" . (new DateTime())->getTimestamp();
$pdf->Output(__DIR__ . "/source/Uploads/pdf/{$nameFile}.pdf", \Mpdf\Output\Destination::FILE);

foreach ($subjectsMessage as $key => $value) {
    if (!!strstr($arrayReversed[0]->status, $key)) {
        $mailer->boostrap($value, $html, $email);
        $_SESSION['email'] = $email;
        echo $mailer->send();
        header("location: sucesso.php");
        return null;
    }
}